using Microsoft.AspNetCore.SignalR;

namespace CS_signalR_backend.Hubs
{
    public class MessageHub : Hub
    {

        static int usersConnected;

        public async Task SendMessage(string color){
            await Clients.All.SendAsync("RecieveMessage",color);
        }

        public override Task OnConnectedAsync()
     {
        usersConnected ++;
        Console.WriteLine(usersConnected);

        return base.OnConnectedAsync();
        
      }

    public override Task OnDisconnectedAsync(Exception exception)
    {
        usersConnected --;
        return base.OnDisconnectedAsync(exception);

    }
    }
}