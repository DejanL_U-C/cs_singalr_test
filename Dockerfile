﻿FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
WORKDIR /app

# Copy everything
COPY . ./

# restore project
RUN dotnet restore


# build app
RUN dotnet build
RUN dotnet publish -c Release -r linux-x64 -o out -p:PublishingSingleFile=true --self-contained


# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app

COPY --from=build-env /app/out .

EXPOSE 3004
EXPOSE 80
ENV ASPNETCORE_URLS=http://+:3004
#RUN dotnet dev-certs https

ENTRYPOINT ["dotnet", "/app/CS_signalR_backend.dll"]
