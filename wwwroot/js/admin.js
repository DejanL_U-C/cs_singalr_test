// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("MessageHub").build();

document.getElementById("sendBlack").disabled = true;
document.getElementById("sendRed").disabled = true;


connection.start().then(()=>{
    document.getElementById("sendBlack").disabled = false;
    document.getElementById("sendRed").disabled = false;
}).catch((err)=>{
    return console.error(err.toString());
});

document.getElementById("sendBlack").addEventListener("click", ()=>{
    let color = "black";
    connection.invoke("SendMessage", color);
    //  TODO other
});
document.getElementById("sendRed").addEventListener("click", ()=>{
    let color = "red";
    connection.invoke("SendMessage", color);
    //  TODO other
});