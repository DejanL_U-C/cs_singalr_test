﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("MessageHub").build();

connection.start().then(()=>{

}).catch((err)=>{
    return console.error(err.toString());
});

connection.on("RecieveMessage", (color)=>{
    console.log("color is  ", color);
    document.getElementById("changingDiv").style.backgroundColor = `${color}`;
});


